import os
import datetime
import json
import time
import sys

import requests
import netifaces
from goprocam import GoProCamera
from goprocam import constants

USER_TOKEN = os.getenv('PUSHOVER_USER_TOKEN')
APP_TOKEN  = os.getenv('PUSHOVER_APP_TOKEN')

debug = True

def send_push(message):
    r = requests.post("https://api.pushover.net/1/messages.json", data = {
        "token": APP_TOKEN,
        "user": USER_TOKEN,
        "message": message,
        })
    print(r.text)
    print(message)

def check_ext(filename):
    ext = os.path.splitext(filename)[1]

    if ext == '.MP4' or ext == '.JPG':
        return True

    return False

def main():
    while True:
        # Checks for an active connection over wlan0, this assumes
        # the only connection will be to the camera i.e. this will
        # run if wlan0 if connected to any network
        if netifaces.AF_INET in netifaces.ifaddresses('wlan0'):
            if debug:
                print("Pi is connected to goPro network")
            # a situation may arise where the ip is still assigned but the
            # gopro is not longer responding to requests. this casues a "Waking
            # up" message and subequent exception???
            try: 
                gpCam = GoProCamera.GoPro()
            except:
                print("issues connecting to camera")
                time.sleep(60*5)

            
            if gpCam.IsRecording():
                if debug:
                    print("goPro is recording, sleeping for 2 minutes")
                time.sleep(60*2)
                continue

            gpCam.downloadAll()
            gpCam.delete("all")


            date = datetime.datetime.today()
            folder = f"{ date.day }-{ date.month }-{ date.year }"
            print(f"making directory { folder } in { os.getcwd() }")
            os.makedirs(folder,exist_ok=True)

            files = list(filter(check_ext, os.listdir()))
            files_len = len(files)
            print(f"moving { files_len } { 'file' if files_len == 1 else 'files' } into { folder }")
            for f in files:
                os.rename(f, f"{folder}/{f}")

            send_push(f"Downloaded { len(files) } files")

            # wait 12 hours to attempt the next connection
            print("sleeping for 12 hours")
            time.sleep(60*60*12)

        else:
            # check for a connection to the camera every minute
            if debug:
                print("goPro connection not established, sleeping for 1 minute")
            time.sleep(60)


try:
    if __name__ == '__main__':
        main()
except KeyboardInterrupt:
    sys.exit()
except AttributeError as e:
    print(f"AttributeError: { e }")
    time.sleep(60*2)
    main()
except:
    print(sys.exc_info()[0])
    send_push("GoPro downloading has failed")
    sys.exit(1)
